var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var autoprefixer = require('autoprefixer');

module.exports = {
    context: path.resolve(__dirname, './src'),
    entry: {
        app: './index.js'
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, './public/')
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [ 'babel-loader' ]
            },
            {
                test:   /\.scss/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader',
                        'sass-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [ autoprefixer ];
                                }
                            }
                        }
                    ]
                })
            }
        ]
    },
    resolve: {
        modules: [
            path.resolve(__dirname),
            'node_modules'
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "bundle.css",
            disable: false,
            allChunks: true
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        publicPath: "/public/",
        compress: true,
        port: 3000
    }
};
