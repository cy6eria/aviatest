import React from 'react';

import { Inputs_Checkbox } from '../Inputs';

export function Filter (props) {
    const { onFilter, onOnlyFilter, onFilterToggle, filterVisible, filter } = props;
    const { stops_0, stops_1, stops_2, stops_3 } = filter;

    return (
        <div className={`filter ${filterVisible ? '' : 'filter--hidden'}`}>
            <div className="filter__trigger" onClick={onFilterToggle}/>
            <div className="filter__panel panel">
                <div className="filter__title">Количество пересадок</div>
                <Inputs_Checkbox
                    id="stops_all"
                    className="filter__checkbox filter__select_all"
                    checked={stops_0 && stops_1 && stops_2 && stops_3}
                    onChange={() => onFilter(['stops_0', 'stops_1', 'stops_2', 'stops_3'])}
                >
                    Все
                </Inputs_Checkbox>
                <Inputs_Checkbox
                    id="stops_0"
                    className="filter__checkbox"
                    checked={stops_0}
                    onChange={() => onFilter(['stops_0'])}
                >
                    Без пересадок
                    <button className="filter__only_button" onClick={() => onOnlyFilter('stops_0')}>
                        только
                    </button>
                </Inputs_Checkbox>
                <Inputs_Checkbox
                    id="stops_1"
                    className="filter__checkbox"
                    checked={stops_1}
                    onChange={() => onFilter(['stops_1'])}
                >
                    1 Пересадка
                    <button className="filter__only_button" onClick={() => onOnlyFilter('stops_1')}>
                        только
                    </button>
                </Inputs_Checkbox>
                <Inputs_Checkbox
                    id="stops_2"
                    className="filter__checkbox"
                    checked={stops_2}
                    onChange={() => onFilter(['stops_2'])}
                >
                    2 Пересадки
                    <button className="filter__only_button" onClick={() => onOnlyFilter('stops_2')}>
                        только
                    </button>
                </Inputs_Checkbox>
                <Inputs_Checkbox
                    id="stops_3"
                    className="filter__checkbox"
                    checked={stops_3}
                    onChange={() => onFilter(['stops_3'])}
                >
                    3 Пересадки
                    <button className="filter__only_button" onClick={() => onOnlyFilter('stops_3')}>
                        только
                    </button>
                </Inputs_Checkbox>
            </div>
        </div>
    )
}
