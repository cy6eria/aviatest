import React from 'react';

export function Inputs_Checkbox (props) {
    const { id, checked, children, className, onChange } = props;
    
    return (
        <div className={`input--checkbox ${className || ''}`}>
            <input id={id} type="checkbox" checked={checked} onChange={(e) => onChange()} />
            <label htmlFor={id}>{children}</label>
        </div>
    );
}
