import React from 'react';
import 'whatwg-fetch';

import { Filter } from '../Filter';
import { List } from '../List';
import { Utils_Loading } from '../Utils';

export class App extends React.Component {
    constructor (props) {
        super(props);

        this.onFilter = this.onFilter.bind(this);
        this.onOnlyFilter = this.onOnlyFilter.bind(this);
        this.onFilterToggle = this.onFilterToggle.bind(this);

        this.state = {
            filterVisible: false,
            filter: {
                stops_0: false,
                stops_1: false,
                stops_2: false,
                stops_3: false
            },
            data: undefined,
            filteredData: []
        }
    }

    componentDidMount () {
        fetch('/tickets.json').then(response => {
            return response.json();
        }).then(json => {
            const tickets = json.tickets.sort(function (a, b) {
                if (a.price > b.price) {
                    return 1;
                }

                if (a.price < b.price) {
                    return -1;
                }

                return 0;
            });

            this.setState({
                data: tickets,
                filteredData: tickets
            });
        })
    }

    onFilterToggle () {
        this.setState({
            filterVisible: !this.state.filterVisible  
        })
    }

    updateFilter (newFilterState) {
        const { data } = this.state;

        this.setState({
            filter: newFilterState,
            filteredData: ((newFilterState) => {
                const { stops_0, stops_1, stops_2, stops_3 } = newFilterState;

                if (stops_0 || stops_1 || stops_2 || stops_3) {
                    return data.filter((ticket) => newFilterState[`stops_${ticket.stops}`]);
                } else {
                    return data.slice(0);
                }
            })(newFilterState)
        });
    }

    onOnlyFilter (key) {
        const { filter } = this.state;
        const newFilterState = Object.assign({}, filter, {
            stops_0: key == 'stops_0',
            stops_1: key == 'stops_1',
            stops_2: key == 'stops_2',
            stops_3: key == 'stops_3'
        });

        this.updateFilter(newFilterState);    
    }

    onFilter (keys) {
        const { filter } = this.state;
        const newFilterState = Object.assign({}, filter);
        const { stops_0, stops_1, stops_2, stops_3 } = newFilterState;
        
        let newValue = undefined;

        if (keys.length > 1) {
            newValue = !(stops_0 && stops_1 && stops_2 && stops_3);
        }

        keys.forEach(key => newFilterState[key] = newValue || !newFilterState[key]);

        this.updateFilter(newFilterState);
    }
    
    render () {
        const { filter, filterVisible, data, filteredData } = this.state;

        return (
            <div className="container">
                <div className="logo">
                    <img src="/images/logo.svg" alt="Aviasales" />
                </div>
                <Filter
                    filter={filter}
                    onFilter={this.onFilter}
                    onOnlyFilter={this.onOnlyFilter}
                    onFilterToggle={this.onFilterToggle}
                    filterVisible={filterVisible}
                />
                {data ? <List tickets={filteredData} /> : <Utils_Loading />}
            </div>
        );
    }
}
