import React from 'react';

import { List_Item } from './List_Item.jsx';

export function List (props) {
    const { tickets } = props;

    return (
        <ul className="list">
            {tickets.map((ticket, key) => <List_Item key={key} ticket={ticket}/>)}
        </ul>
    )
}