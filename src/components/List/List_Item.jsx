import React from 'react';

function formatDate (date) {
    const months = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
    const daysOfWeek = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
    const [day, month, year] = date.split('.');
    const dateObj = new Date('20' + year, month-1, day);
    
    return `${day} ${months[dateObj.getMonth()]} 20${year}, ${daysOfWeek[dateObj.getDay() - 1]}`;
}

function formatTime (time) {
    const [hours, minutes] = time.split(':');

    return `${hours.length === 1 ? '0'+hours : hours}:${minutes}`;
}

function formatStops (stops) {
    if (stops == 0) {
        return '';
    }

    if (stops == 1) {
        return '1 пересадка';
    }

    return `${stops} пересадки`;
}

export function List_Item (props) {
    const { ticket } = props;

    return (
        <li className="panel list_item">
            <div className="list_item__buy_section">
                <img src={`https://pics.avs.io/99/36/${ticket.carrier}@2x.png`} alt={ticket.carrier} />

                <button className="list_item__button">
                    Купить<br/>за {ticket.price} Р
                </button>
            </div>
            <div className="list_item__info_section">
                <div className="list_item__stops">
                    &nbsp;{formatStops(ticket.stops)}
                    <img src="/images/plan.svg"/>
                </div>
                <div className="list_item__departure_info">
                    <div className="list_item__time">
                        {formatTime(ticket.departure_time)}
                    </div>
                    <div className="list_item__airport">
                        {ticket.origin}, {ticket.origin_name}
                    </div>
                    <div className="list_item__date">
                        {formatDate(ticket.departure_date)}
                    </div>
                </div>
                <div className="list_item__arrival_info">
                    <div className="list_item__time">
                        {formatTime(ticket.departure_time)}
                    </div>
                    <div className="list_item__airport">
                        {ticket.destination}, {ticket.destination_name}
                    </div>
                    <div className="list_item__date">
                        {formatDate(ticket.arrival_date)}
                    </div>
                </div>
            </div>
        </li>
    );
}
